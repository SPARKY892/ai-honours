#include "game.h"
#include "StateMainMenu.h"
#include "SimpleAIState.h"
#include "AdvancedAIState.h"
#include "label.h"
#include "GameState.h"
#include "creditState.h"

int roomWidth;
int roomHeight;

Game::Game()
{
	roomHeight = 768;
	roomWidth = 928;
}

Game::~Game()
{
    SDL_DestroyWindow(window);
    SDL_Quit();
}

// We should be able to detect when errors occur with SDL if there are 
// unrecoverable errors, then we need to print an error message and quit the program
void exitFatalError(char *message)
{
    std::cout << message << " " << SDL_GetError();
    SDL_Quit();
    exit(1);
}

// Set up rendering context
// Sets values for, and creates an OpenGL context for use with SDL
SDL_Window * Game::setupRC(SDL_GLContext &context)
{
	SDL_Window *window;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        exitFatalError("Unable to initialize SDL"); 

    // Request an OpenGL 2.1 context.
	// If you request a context not supported by your drivers, no OpenGL context will be created
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering

	// Optional: Turn on x4 multisampling anti-aliasing (MSAA)
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
 
    // Create 800x640 window
    window = SDL_CreateWindow("AI Honours Project",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        roomWidth, roomHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate

	glOrtho(0, roomWidth, roomHeight, 0, 0, 1);

	if (TTF_Init() == -1)
		exitFatalError("TTF Failed to initialise.");

	return window;
}

void Game::init(void)
{
	srand((int)time(0));
	SDL_GLContext glContext; // OpenGL context handle
    window = setupRC(glContext); // Create window and render context
	
}

void Game::run(void)
{
	bool running = true; // set running to true
	SDL_Event sdlEvent; // variable to detect SDL events

	while (running)	// the event loop
	{
		while (SDL_PollEvent(&sdlEvent))
		{
			if (sdlEvent.type == SDL_QUIT)
				running = false;
			else
				State->handleSDLEvent(sdlEvent, *this);
			
		}

		State->update(*this);	
		State->draw(window); // call the draw function
	}
}

// state transition function
void Game::setState(GameState* newState)
{
	if(newState != State)  
	{
		State = newState;
		State->init(*this);
		State->enter(); //perform Enter actions on new state
	}
}
