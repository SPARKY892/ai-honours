#include "game.h"
#include "StateMainMenu.h"
#include "SimpleAIState.h"
#include "AdvancedAIState.h"
#include "label.h"
#include <iostream>
#include <cstdlib>

StateMainMenu::StateMainMenu() {
	playLabel = new Label();
	continueLabel = new Label();
	quitLabel = new Label();
	creditLabel = new Label();
	TTF_Init();

	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);

	playLabel->textToTexture("New Game [N]", textFont);
	continueLabel->textToTexture("Continue Game [C]", textFont);
	quitLabel->textToTexture("Quit [Q/Esc]", textFont);
	creditLabel->textToTexture("Credits [R]", textFont);
}

void StateMainMenu::enter(){
	
}

void StateMainMenu::Exit(){
	
}

void StateMainMenu::draw(SDL_Window * window){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f ,0.0f,0.0f, 0.0f);

	playLabel->draw(roomWidth / 2 - 100.0f, roomHeight / 2 - 200.0f);
	continueLabel->draw(roomWidth / 2 - 100.0f, roomHeight / 2 - 100.0f);
	quitLabel->draw(roomWidth / 2 - 100.0f, roomHeight / 2);
	creditLabel->draw(roomWidth / 2 - 100.0f, roomHeight / 2 + 100.0f);

	SDL_GL_SwapWindow(window);
}

void StateMainMenu::handleSDLEvent(SDL_Event const &sdlEvent, Game &context){
	if (sdlEvent.type == SDL_KEYDOWN){
		switch( sdlEvent.key.keysym.sym )
		{
		case SDLK_n:
			context.setState(selectState); 
			break;
		case SDLK_c:

			if(((SimpleAIState*)simpleAIState)->getCanContinue())
			{
				context.setState(simpleAIState);
			}
			else if (((AdvancedAIState*)advancedAIState)->getCanContinue())
			{
				context.setState(advancedAIState);
			}
				
			break;
		case SDLK_r:
			std::cout << "pressed R" << std::endl;
			context.setState(creditState);
			break;
		case SDLK_ESCAPE: case SDLK_q:
			exit(1);
			break;
		default:
			break;
		}
	}
}

void StateMainMenu::init(Game * context){
	this->init(context);
}

void StateMainMenu::init(Game &context){
	
}

StateMainMenu::~StateMainMenu() {
	delete playLabel;
	delete continueLabel;
	delete quitLabel;
	delete creditLabel;
}