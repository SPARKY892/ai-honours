#include "Item.h"
#include "label.h"
#include "Player.h"

Item::Item(Player * player)
{
	this->player = player;
	dropChance = 0;
	itemID = 0;

	xPos = 0.0f;
	yPos = 0.0f;
	width = 0.05f;
	height = 0.05f;
}

float Item::getX()
{
	return xPos;
}

float Item::getY()
{
	return yPos;
}

float Item::getWidth()
{
	return width;
}

float Item::getHeight()
{
	return height;
}

void Item::setX(float x)
{
	xPos = x;
}

void Item::setY(float y)
{
	yPos = y;
}