#pragma once
#include "Common.h"
#include "Player.h"
#include "Wall.h"
#include "monster.h"

class Bullet
{
public:
	Bullet(Player *, vector<Wall *>&, vector<Monster *>&,direction dir, bool cameFromPlayer, Monster * monster = NULL);
	
	void draw();
	bool collisionCheck();
	void update();

	int getX() { return x; }
	int getY() { return y; }
	int getWidth() { return width; }
	int getHeight() { return height; }
	void setBulletOrigin(SquareGrid::Location origin) { bulletOrigin = origin; }

private:
	Player * player;
	Monster * monster;
	int x, y, width, height;
	direction dir;
	vector<Wall *> walls;
	vector<Monster *> monsters;
	bool collided;
	bool cameFromPlayer;
	SquareGrid::Location bulletOrigin;
};

