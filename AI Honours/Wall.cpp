#include "Wall.h"

Wall::Wall(Player * player, int x, int y, int width, int height)
{
	this->player = player;
	this->x = x * 32;
	this->y = y * 32;
	this->width = width * 32;
	this->height = height * 32;
}

void Wall::draw()
{
	// draw target
	glColor3f(0.2, 0.2, 0.2);
	glBegin(GL_POLYGON);
	glVertex3f(this->getX(), this->getY(), 0.0); // first corner
	glVertex3f(this->getX() + this->getWidth(), this->getY(), 0.0); // second corner
	glVertex3f(this->getX() + this->getWidth(), this->getY() + this->getHeight(), 0.0); // third corner
	glVertex3f(this->getX(), this->getY() + this->getHeight(), 0.0); // fourth corner
	glEnd();

}
