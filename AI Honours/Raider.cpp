#include "Raider.h"

Raider::Raider(Player * player) : Monster(player)
{
	this->player = player;

	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	targetText = new Label();

	setHealth(4);
	setStrength(1);
	setTypeID(2);
	setGoal(player->getLocation());
	setRange(4);

	clockStartTime = clock();
	bulletFireTime = clock();
	moveToLastLoc = false;
	justReturned = false;
	waiting = false;
}

void Raider::draw()
{
	//draws monster depending on current direction
	if (getDir() == UP)
	{
		glColor3f(0.0f, 0.0f, 1.0f);
		glBegin(GL_TRIANGLES);
		glVertex2f(getX(), getY() + getHeight());
		glVertex2f(getX() + getWidth(), getY() + getHeight());
		glVertex2f(getX() + getWidth() / 2, getY());
		glEnd();
		
		point1.x = getX() + getWidth() / 2; point1.y =  getY();
		point2.x = getX() - getWidth() / 2; point2.y = getY() - 5 * getHeight();
		point3.x = getX() + getWidth() + getWidth() / 2; point3.y = getY() - 5 * getHeight();
		
	}
	else if (getDir() == LEFT)
	{
		glColor3f(0.0f, 0.0f, 1.0f);
		glBegin(GL_TRIANGLES);
		glVertex2f(getX() + getWidth(), getY());
		glVertex2f(getX() + getWidth(), getY() + getHeight());
		glVertex2f(getX(), getY() + getHeight() / 2);
		glEnd();

	
		point1 = { getX(), getY() + getHeight() / 2 };
		point2 = { getX() - 5 * getWidth(), getY() + getHeight() + getHeight() / 2 };
		point3 = { getX() - 5 * getWidth(), getY() - getHeight() / 2 };
	}
	else if (getDir() == DOWN)
	{
		glColor3f(0.0f, 0.0f, 1.0f);
		glBegin(GL_TRIANGLES);
		glVertex2f(getX(), getY());
		glVertex2f(getX() + getWidth(), getY());
		glVertex2f(getX() + getWidth() / 2, getY() + getHeight());
		glEnd();

		point1 = { getX() + getWidth() / 2, getY() + getHeight() };
		point2 = { getX() + getWidth() + getWidth() / 2, getY() + 6 * getHeight() };
		point3 = { getX() - getWidth() / 2, getY() + 6 * getHeight() };
		
		
	}
	else if (getDir() == RIGHT)
	{
		glColor3f(0.0f, 0.0f, 1.0f);
		glBegin(GL_TRIANGLES);
		glVertex2f(getX(), getY());
		glVertex2f(getX(), getY() + getHeight());
		glVertex2f(getX() + getWidth(), getY() + getHeight() / 2);
		glEnd();

		point1 = { getX() + getWidth(), getY() + getHeight() / 2 };
		point2 = { getX() + 6 * getWidth(), getY() + getHeight() + getHeight() / 2 };
		point3 = { getX() + 6 * getWidth(), getY() - getHeight() / 2 };
	}
	
	if (getSpottedPlayer())
	{
		glColor4f(1.0f, 0.0f, 0.0f, 0.35f);
	}
	else
	{
		glColor4f(1.0f, 1.0f, 0.0f, 0.35f);
	}
	glBegin(GL_TRIANGLES);
	glVertex2f(point1.x, point1.y);
	glVertex2f(point2.x, point2.y);
	glVertex2f(point3.x, point3.y);
	glEnd();

	//selects colour for health bar depending on health value
	switch (getHealth())
	{
	case 4:
		glColor3f(0.0f, 1.0f, 0.0f);
		break;
	case 3:
		glColor3f(1.0f, 0.87f, 0.0f);
		break;
	case 2:
		glColor3f(1.0f, 0.65f, 0.0f);
		break;
	case 1:
		glColor3f(1.0f, 0.0f, 0.0f);
	}

	//draw health bar
	glBegin(GL_POLYGON);
	glVertex2f(getX(), getY() - 10);
	glVertex2f(getX() + (getHealth() * 8), getY() - 10);
	glVertex2f(getX() + (getHealth() * 8), getY() - 5);
	glVertex2f(getX(), getY() - 5);
	glEnd();
}

bool Raider::collisionCheck()
{
	//checks for collisions with the player
	if (player->getX() <= getX() + getWidth() && player->getX() + player->getWidth() >= getX()) {
		if (player->getY() <= getY() + getHeight() && player->getY() + player->getHeight() >= getY()) {
			player->takeDamage(1);
			return true;
		}
	}

	return false;
}

float sign(point p1, point p2, point p3)
{
	return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

bool Raider::inLineOfSight(point p1, point p2, point p3, point m1, point m2, point m3)
{
	bool b1, b2, b3;

	b1 = sign(p1, m1, m2) < 0.0f;
	b2 = sign(p1, m2, m3) < 0.0f;
	b3 = sign(p1, m3, m1) < 0.0f;

	if (b1 == b2 && b2 == b3)
	{
		return true;
	}

	b1 = sign(p2, m1, m2) < 0.0f;
	b2 = sign(p2, m2, m3) < 0.0f;
	b3 = sign(p2, m3, m1) < 0.0f;

	if (b1 == b2 && b2 == b3)
	{
		return true;
	}

	b1 = sign(p3, m1, m2) < 0.0f;
	b2 = sign(p3, m2, m3) < 0.0f;
	b3 = sign(p3, m3, m1) < 0.0f;

	if (b1 == b2 && b2 == b3)
	{
		return true;
	}

	return false;
}

bool Raider::fireBullet()
{
	clock_t diff = clock() - bulletFireTime;
	int ms = diff * 1000 / CLOCKS_PER_SEC;

	GridWithWeights tempGrid = player->getGrid();

	if (inLineOfSight(player->getPoint1(), player->getPoint2(), player->getPoint3(), getPoint1(), getPoint2(), getPoint3()) && ms >= 400)
	{
		bulletFireTime = clock();
		return true;
	}
	return false;
}

void Raider::update()
{
	//Checks time between start of timer and current time
	clock_t diff = clock() - clockStartTime;
	int ms = diff * 1000 / CLOCKS_PER_SEC;

	//checks if player is next to monster and turns to face them
	if (distance(player->getLocation()) == 1)
	{
		if (player->getX() < getX())
		{
			setDir(LEFT);
		}
		else if (player->getX() > getX())
		{
			setDir(RIGHT);
		}
		else if (player->getY() < getY())
		{
			setDir(UP);
		}
		else if (player->getY() > getY())
		{
			setDir(DOWN);
		}
	}

	GridWithWeights tempGrid = player->getGrid();

	if (ms >= 500 && justStarted)
	{
		justStarted = false;
	}
	
	//Move to Player if in range
	if ((inLineOfSight(player->getPoint1(), player->getPoint2(), player->getPoint3(), getPoint1(), getPoint2(), getPoint3()) || distance(player->getLocation()) <= getRange() || getSpottedPlayer()) && !onWaypoint && getHealth() > 2)
	{

		setSpottedPlayer(true);

		if (distance(player->getLocation()) >= 8 || justStarted)
		{
			setSpottedPlayer(false);
		}

		if (player->getLocation() != getGoal()) {
			setStart(getLocation());
			setGoal(player->getLocation());
			getPath();
		}
		
		if (ms >= 100) {
			advancePath();
			clockStartTime = clock();
		}
	}
	//Moves to waypoint when low on health
	else if (getHealth() <= 2 || onWaypoint) {
		if (!hasPath) {
			int smallestDist = 1000;

			for (auto i = tempGrid.waypoints.begin(); i != tempGrid.waypoints.end(); i++) {
				SquareGrid::Location tempLoc = *i;

				if (distance(tempLoc) < smallestDist) {
					waypoint = tempLoc;
					smallestDist = distance(tempLoc);
				}
			}

			setStart(getLocation());
			setGoal(waypoint);
			lastLoc = getLocation();
			getPath();
			advancePath();
			hasPath = true;
		}
		else if (hasPath && ms >= 100) {
			if (getLocation() == waypoint) {
				onWaypoint = true;
			}

			if (!onWaypoint) {
				advancePath();
				clockStartTime = clock();
			}
			else if(onWaypoint && getHealth() < 4){
				setHealth(1);
				clockStartTime = clock();
			}
			else if (onWaypoint && getHealth() == 4) {
				onWaypoint = false;
				hasPath = false;
				moveToLastLoc = true;
				setStart(getLocation());
				setGoal(lastLoc);
				getPath();
				advancePath();
				clockStartTime = clock();
			}
		}
	}
	//if taken damage move to loacation bullet was fired from
	else if (getHealth() == 3 && !onWaypoint && !moveToLastLoc && !justReturned)
	{
		if (!investigate && !waiting)
		{
			lastLoc = getLocation();
			setStart(getLocation());
			setGoal(getBulletOrigin());
			getPath();
			advancePath();
			investigate = true;
		}
		else if (investigate && ms >= 100 && !waiting)
		{
			advancePath();
			clockStartTime = clock();
		}

		if (getLocation() == getGoal())
		{
			waiting = true;

			if (waitingTurn < 4 && ms >= 500)
			{
					std::random_device rd;
					std::mt19937 engine{ rd() };
					std::uniform_int_distribution<int> dist(0, waitingDir.size() - 1);
					int random = dist(engine);

					setDir(waitingDir[random]);
					waitingDir.erase(waitingDir.begin() + random);
					waitingTurn++;
					clockStartTime = clock();
			}
			else if (!inLineOfSight(player->getPoint1(), player->getPoint2(), player->getPoint3(), getPoint1(), getPoint2(), getPoint3()) && waitingTurn == 4 && ms >= 500)
			{
				moveToLastLoc = true;
				setStart(getLocation());
				setGoal(lastLoc);
				getPath();
				advancePath();
				clockStartTime = clock();
				waiting = false;
				waitingDir = { UP, DOWN, LEFT, RIGHT };
				waitingTurn = 0;
			}

			investigate = false;
		}

	}
	//move to last location beofre taking cover
	else if (moveToLastLoc) {
		if (getLocation() == lastLoc) {
			if (getHealth() == 3) {
				justReturned = true;
			}
			else
			{
				justReturned = false;
			}

			moveToLastLoc = false;
			//setIdle(true);
		}
		else if (ms >= 100) {
			advancePath();
			clockStartTime = clock();
		}

	}
}
