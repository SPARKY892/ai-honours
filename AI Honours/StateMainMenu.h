#pragma once

#include "Common.h"
#include "GameState.h"
#include "label.h"

class StateMainMenu: public GameState
{
public:
	StateMainMenu();
	void draw(SDL_Window * window);
	void init(Game * context);
	void init(Game &context);
	void update(Game &context){ };
	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);
	void enter();
	void Exit();
	~StateMainMenu();
	


private:
	Label * playLabel;
	Label * continueLabel;
	Label * quitLabel;
	Label * creditLabel;
	TTF_Font* textFont;	// SDL type for True-Type font rendering
	Game * context;	
};