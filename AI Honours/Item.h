#pragma once
#include "Common.h"
#include "Player.h"

class Item
{
public:

	Item(Player*);
	~Item() { }

	virtual void draw() = 0;
	virtual int update() = 0;
	
	void setX(float);
	void setY(float);

	float getX();
	float getY();
	float getWidth();
	float getHeight();

protected:

	Player * player;
	float xPos, yPos, width, height;
	int dropChance;
	int itemID;
};

