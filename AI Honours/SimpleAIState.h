#pragma once

#include "Common.h"
#include "GameState.h"
#include"label.h"
#include "Player.h"
#include "Raider.h"
#include "Brute.h"
#include "ItemHealthPack.h"
#include "Wall.h"
#include "Bullet.h"

class SimpleAIState: public GameState
{
public: 
	SimpleAIState();
	~SimpleAIState();
	void draw(SDL_Window *window);
	void init(Game * context);
	void init(Game &context);
	void update(Game &context);
	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);
	void enter();
	void Exit();
	int nearestMult(int number, int multiple);

	void setCanContinue(bool canCont) { canContinue = canCont; };
	bool getCanContinue(){ return canContinue; };
	bool getGameFinished();

	int levelWidth = 800;
	int levelHeight = 600;

private:
	
	int deadMonsterCount;
	Game * context;
	Player * player;
	vector<Monster *> monsters;
	vector <Wall *> walls;
	vector <Bullet *> bullets;

	bool canContinue;
	bool gameStarted;
	int monsterToDelete;
	int itemToDelete;
	int score;
	clock_t lastTime; // clock_t is an integer type
	clock_t currentTime; // use this to track time between frames
	TTF_Font* textFont;	// SDL type for True-Type font rendering
	Label * scoreText;

	
	
};