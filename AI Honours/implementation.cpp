/*
 Sample code from http://www.redblobgames.com/pathfinding/
 Copyright 2014 Red Blob Games <redblobgames@gmail.com>
 
 Feel free to use this code in your own projects, including commercial projects
 License: Apache v2.0 <http://www.apache.org/licenses/LICENSE-2.0.html>
*/

#include "implementation.h"

// This outputs a grid. Pass in a distances map if you want to print
// the distances, or pass in a point_to map if you want to print
// arrows that point to the parent location, or pass in a path vector
// if you want to draw the path.
//template<class Graph>
//void draw_grid(const Graph& graph, int field_width, 
//               unordered_map<typename Graph::Location, int>* distances=nullptr,
//               unordered_map< Graph::Location, typename Graph::Location>* point_to=nullptr,
//               vector<typename Graph::Location>* path=nullptr) {
//  for (int y = 0; y != graph.height; ++y) {
//    for (int x = 0; x != graph.width; ++x) {
//      typename Graph::Location id {x, y};
//      std::cout << std::left << std::setw(field_width);
//      if (graph.walls.count(id)) {
//        std::cout << string(field_width, '#');
//      } else if (point_to != nullptr && point_to->count(id)) {
//        int x2, y2;
//        tie (x2, y2) = (*point_to)[id];
//        // TODO: how do I get setw to work with utf8?
//        if (x2 == x + 1) { std::cout << "\u2192 "; }
//        else if (x2 == x - 1) { std::cout << "\u2190 "; }
//        else if (y2 == y + 1) { std::cout << "\u2193 "; }
//        else if (y2 == y - 1) { std::cout << "\u2191 "; }
//        else { std::cout << "* "; }
//      } else if (distances != nullptr && distances->count(id)) {
//        std::cout << (*distances)[id];
//      } else if (path != nullptr && find(path->begin(), path->end(), id) != path->end()) {
//        std::cout << '@';
//      } else {
//        std::cout << '.';
//      }
//    }
//    std::cout << std::endl;
//  }
//}


array<SquareGrid::Location, 4> SquareGrid::DIRS {Location{1, 0}, Location{0, -1}, Location{-1, 0}, Location{0, 1}};


void add_rect(SquareGrid& grid, int x, int y, int width, int height) {
  for (int i = x; i < x + width; ++i) {
    for (int j = y; j < y + height; ++j) {
      grid.walls.insert(SquareGrid::Location { i, j });
    }
  }
}

SquareGrid make_diagram1() {
  SquareGrid grid(30, 15);
  add_rect(grid, 3, 3, 5, 12);
  add_rect(grid, 13, 4, 15, 15);
  add_rect(grid, 21, 0, 23, 7);
  add_rect(grid, 23, 5, 26, 7);
  return grid;
}

GridWithWeights make_diagram4() {
	GridWithWeights grid(29, 24);
	
	//add the walls to the grid
	add_rect(grid, 0, 0, 1, 24);
	add_rect(grid, 0, 0, 29, 1);
	add_rect(grid, 28, 0, 1, 24);
	add_rect(grid, 0, 23, 29, 1);
	
	add_rect(grid, 3, 8, 6, 1);
	add_rect(grid, 8, 3, 1, 6);

	add_rect(grid, 20, 8, 6, 1);
	add_rect(grid, 20, 3, 1, 6);

	add_rect(grid, 11, 11, 7, 3);

	add_rect(grid, 3, 15, 6, 1);
	add_rect(grid, 8, 15, 1, 6);

	add_rect(grid, 20, 15, 6, 1);
	add_rect(grid, 20, 15, 1, 6);

	typedef SquareGrid::Location L;
	grid.waypoints = unordered_set<SquareGrid::Location> { 
	L{7, 7}, L{21, 7}, L{7, 16}, L{21, 16}
	};
	return grid;
}

//template<typename Graph>
//void dijkstra_search
//  (const Graph& graph,
//   typename Graph::Location start,
//   typename Graph::Location goal,
//   unordered_map<typename Graph::Location, typename Graph::Location>& came_from,
//   unordered_map<typename Graph::Location, int>& cost_so_far)
//{
//  typedef typename Graph::Location Location;
//  PriorityQueue<Location> frontier;
//  frontier.put(start, 0);
//
//  came_from[start] = start;
//  cost_so_far[start] = 0;
//  
//  while (!frontier.empty()) {
//    auto current = frontier.get();
//
//    if (current == goal) {
//      break;
//    }
//
//    for (auto next : graph.neighbors(current)) {
//      int new_cost = cost_so_far[current] + graph.cost(current, next);
//      if (!cost_so_far.count(next) || new_cost < cost_so_far[next]) {
//        cost_so_far[next] = new_cost;
//        came_from[next] = current;
//        frontier.put(next, new_cost);
//      }
//    }
//  }
//}

