#pragma once
#include "monster.h"
#include "Bullet.h"
#include <random>

class Raider : public Monster
{
public:
	Raider(Player *);
	void draw();
	bool collisionCheck();
	void update();
	bool fireBullet();

	bool moved = false;
	clock_t clockStartTime;
	clock_t bulletFireTime;
	int count;
	bool inLineOfSight(point p1, point p2, point p3, point m1, point m2, point m3);
	point getPoint1() { return point1; }
	point getPoint2() { return point2; }
	point getPoint3() { return point3; }

	

private:
	SquareGrid::Location lastLoc;
	SquareGrid::Location waypoint;
	bool onWaypoint;
	bool hasPath;
	bool moveToLastLoc;
	bool investigate;
	bool justReturned;
	bool waiting;
	bool justStarted = true;
	Player * player;
	Label * targetText;
	TTF_Font * textFont;
	vector <Bullet *> bullets;
	point point1;
	point point2;
	point point3;


	vector<direction> waitingDir = { UP, DOWN, LEFT, RIGHT };
	int waitingTurn = 0;
};
