#pragma once

#include "Common.h"
#include "label.h"
#include "Player.h"


class Monster{
public:
	Monster(Player*);
	virtual void draw() = 0;
	virtual void update() = 0;
	virtual bool collisionCheck() = 0;
	virtual bool fireBullet() = 0;

	int getX();
	int getY();
	int getWidth();
	int getHeight();
	int getStrength();
	int getSpeed();
	int getHealth();
	int getDollarDrop();
	int getTypeID();
	int getDropChance();


	void setX(int x);
	void setY(int y);
	void setHealth(int);
	void setStrength(int);
	void setSpeed(int);
	void setDollarDrop(int);
	void takeDamage(int);
	void setTypeID(int);
	void setDropChance(int);

	void setGoal(SquareGrid::Location loc);
	void setStart(SquareGrid::Location loc);
	void getPath();

	SquareGrid::Location getNextLocation();
	bool gotPath();
	void setPathBool(bool);

	SquareGrid::Location getGoal() { return goal; }
	SquareGrid::Location getStart() { return start; }
	float distance(SquareGrid::Location targetLoc);

	float getRange() { return range; }
	void setRange(float r) { range = r; }
	direction getDir() { return currentDir; }

	void setDir(direction);
	SquareGrid::Location getLocation() { return std::make_tuple(getX() / 32, getY() / 32); }
	void advancePath();

	void setBulletOrigin(SquareGrid::Location origin) { bulletOrigin = origin; }
	SquareGrid::Location getBulletOrigin() { return bulletOrigin; }

	bool spawnCheck();
	void setSpottedPlayer(bool spotted) { spottedPlayer = spotted; }
	bool getSpottedPlayer() { return spottedPlayer; }

private:
	int health;
	int strength;
	int speed;
	int dollarDrop;
	int typeID;
	int XPos, YPos, width, height;
	int dropChance;
	bool gotPathBool = false;
	bool spottedPlayer;

	SquareGrid::Location bulletOrigin;
	
	Player * player;
	TTF_Font* textFont;	// SDL type for True-Type font rendering

	SquareGrid::Location goal;
	SquareGrid::Location start;
	vector<SquareGrid::Location> path;
	float range;
	direction currentDir;
	
};