#pragma once

#include "Common.h"
#include "GameState.h"

extern GameState * simpleAIState;
extern GameState * advancedAIState;
extern GameState * mainState;
extern GameState * creditState;
extern GameState * battleFinishState;
extern GameState * splashState;
extern GameState * selectState;

extern int roomHeight;
extern int roomWidth;

class Game
{
public:
	Game();
	
	void init(void);
	void run(void);
	void setState(GameState *);
	
	~Game();

private:
	SDL_Window * setupRC(SDL_GLContext &);
	TTF_Font* textFont;	// SDL type for True-Type font rendering
	SDL_Window *window;
	GameState * State;
};