#pragma once
#include "monster.h"
#include "Bullet.h"

class Brute: public Monster
{
public:
	Brute(Player *);
	void draw();
	bool collisionCheck();
	void update();
	bool fireBullet();

	bool moved = false;
	clock_t startTime;
	clock_t bulletFireTime;

private:
	Player * player;
	Label * targetText;
	TTF_Font * textFont;
	vector <Bullet *> bullets;
};

