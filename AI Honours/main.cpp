#include "game.h"
#include "label.h"
#include "Common.h"
#include "StateMainMenu.h"
#include "SimpleAIState.h"
#include "AdvancedAIState.h"
#include "creditState.h"
#include "BattleFinishState.h"
#include "SplashState.h"
#include "StateSelect.h"

GameState * simpleAIState;
GameState * advancedAIState;
GameState * mainState;
GameState * creditState;
GameState * battleState;
GameState * battleFinishState;
GameState * splashState;
GameState * selectState;

#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

// Program entry point
// SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[])
{
	Game *newGame = new Game();

	newGame->init();
	simpleAIState = new SimpleAIState();
	advancedAIState = new AdvancedAIState();
	mainState = new StateMainMenu();
	creditState = new StateCredits();
	battleFinishState = new BattleFinishState();
	splashState = new SplashState();
	selectState = new SelectState();

	newGame->setState(splashState);
	newGame->run();

	delete newGame;
	return 0;
}