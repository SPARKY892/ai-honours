#include "Player.h"

Player::Player()
{
	xpos  = 4 * 32;
	ypos = 10 * 32;
	width = 1 * 32;
	height = 1 * 32;
	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	playerText = new Label();

	health = 100;
	strength = 10;
	speed = 10;
	dollars = 0;
	maxHealth = 10;
}

void Player::drawPlayer()
{
	// draw player depending on current direction
	glColor3f(1.0f, 0.0f, 0.0f);
	if (this->getDir() == UP)
	{
		point1 = { xpos, ypos + height };
		point2 = { xpos + width / 2, ypos };
		point3 = { xpos + width, ypos + height };
	}
	else if (this->getDir() == LEFT)
	{
		point1 = { xpos + width, ypos };
		point2 = { xpos + width, ypos + height };
		point3 = { xpos, ypos + height / 2 };
	}
	else if (this->getDir() == DOWN)
	{
		point1 = { xpos, ypos };
		point2 = { xpos + width, ypos };
		point3 = { xpos + width / 2, ypos + height };
	}
	else if (this->getDir() == RIGHT)
	{
		point1 = { xpos, ypos };
		point2 = { xpos + width, ypos + height / 2 };
		point3 = { xpos, ypos + height };
	}
	
	glBegin(GL_TRIANGLES);
	glVertex2f(point1.x, point1.y);
	glVertex2f(point2.x, point2.y);
	glVertex2f(point3.x, point3.y);
	glEnd();

	//choose colour for health bar depending on current health
	if (getHealth() > 75)
	{
		glColor3f(0.0f, 1.0f, 0.0f);
	}
	else if (getHealth() > 50 && getHealth() < 76)
	{
		glColor3f(1.0f, 0.87f, 0.0f);
	}
	else if (getHealth() > 25 && getHealth() < 51)
	{
		glColor3f(1.0f, 0.65f, 0.0f);
	}
	else if (getHealth() > 0 && getHealth() < 26)
	{
		glColor3f(1.0f, 0.0f, 0.0f);
	}

	//draw health bar
	glBegin(GL_POLYGON);
	glVertex2f(getX(), getY() - 10);
	glVertex2f(getX() + (getHealth() / (100/32)), getY() - 10);
	glVertex2f(getX() + (getHealth() / (100/32)), getY() - 5);
	glVertex2f(getX(), getY() - 5);
	glEnd();

}

void Player::moveX(int dx)
{
	int tempX = this->getX() + dx;
	tuple<int, int> tempLoc{ tempX / 32, this->getY() / 32 };

	if (gameGrid.walls.count(tempLoc) == 0)
	{
		xpos += dx;
	}
}

void Player::moveY(int dy)
{
	int tempY = this->getY() + dy;
	tuple<int, int> tempLoc { this->getX() / 32, tempY / 32 };

	if (gameGrid.walls.count(tempLoc) == 0)
	{
		ypos += dy;
	}
}

int Player::getX()
{
	return xpos;
}

int Player::getY()
{
	return ypos;
}

int Player::getWidth()
{
	return width;
}

int Player::getHeight()
{
	return height;
}

int Player::getHealth()
{
	return health;
}

void Player::takeDamage(int damage)
{
	health -= damage;
}

void Player::healthPack()
{
	if(health == maxHealth)
	{
		maxHealth += 1;
		health = maxHealth;
	}else
	{
		health = maxHealth;
	}
}