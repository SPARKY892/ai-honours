#include "Bullet.h"

Bullet::Bullet(Player * player, vector<Wall *> &walls, vector<Monster *> &monsters, direction dir, bool cameFromPlayer, Monster * monster)
{
	this->player = player;
	this->cameFromPlayer = cameFromPlayer;

	x = player->getX();
	y = player->getY();

	if (cameFromPlayer)
	{
		setBulletOrigin(player->getLocation());
	}

	if (monster != NULL)
	{
		this->monster = monster;
		x = monster->getX();
		y = monster->getY();
	}

	width = 8;
	height = 8;
	this->dir = dir;
	this->walls = walls;
	this->monsters = monsters;

	if (dir == UP) { x += 12; }
	if (dir == DOWN) { x += 12; y += 24; }
	if (dir == LEFT) { y += 12; }
	if (dir == RIGHT) { x += 24; y += 12; }
}

void Bullet::draw()
{

	glColor3f(0.3, 0.3, 0.3);
	
	if (dir == UP)
	{
		glBegin(GL_POLYGON);
		glVertex3f(getX(), getY(), 0.0); // first corner
		glVertex3f(getX() + getWidth(), getY(), 0.0); // second corner
		glVertex3f(getX() + getWidth(), getY() + getHeight(), 0.0); // third corner
		glVertex3f(getX(), getY() + getHeight(), 0.0); // fourth corner
		glEnd();
	}

	if (dir == DOWN)
	{ 
		glBegin(GL_POLYGON);
		glVertex3f(getX(), getY(), 0.0); // first corner
		glVertex3f(getX() + getWidth(), getY(), 0.0); // second corner
		glVertex3f(getX() + getWidth(), getY() + getHeight(), 0.0); // third corner
		glVertex3f(getX(), getY() + getHeight(), 0.0); // fourth corner
		glEnd();
	}

	if (dir == LEFT)
	{
		glBegin(GL_POLYGON);
		glVertex3f(getX(), getY(), 0.0); // first corner
		glVertex3f(getX() + getWidth(), getY(), 0.0); // second corner
		glVertex3f(getX() + getWidth(), getY() + getHeight(), 0.0); // third corner
		glVertex3f(getX(), getY() + getHeight(), 0.0); // fourth corner
		glEnd();
	}

	if (dir == RIGHT)
	{
		glBegin(GL_POLYGON);
		glVertex3f(getX(), getY(), 0.0); // first corner
		glVertex3f(getX() + getWidth(), getY(), 0.0); // second corner
		glVertex3f(getX() + getWidth(), getY() + getHeight(), 0.0); // third corner
		glVertex3f(getX(), getY() + getHeight(), 0.0); // fourth corner
		glEnd();
	}
}

void Bullet::update()
{
	if (dir == UP)
	{
		y -= 5;
	}
	else if (dir == DOWN)
	{
		y += 5;
	}
	else if (dir == RIGHT)
	{
		x += 5;
	}
	else if (dir == LEFT)
	{
		x -= 5;
	}

}

//checks for collisions with walls and enemies and returns true if a collision occurs
bool Bullet::collisionCheck()
{
	if (cameFromPlayer)
	{
		//checks for collisions with enemies
		for (int i = 0; i < monsters.size(); i++)
		{
			if (this->getX() + this->getWidth() >= monsters[i]->getX() && this->getX() <= monsters[i]->getX() + monsters[i]->getWidth()) {
				if (this->getY() + this->getHeight() >= monsters[i]->getY() && this->getY() <= monsters[i]->getY() + monsters[i]->getHeight()) {
					//reduce monsters health by 1 point
					monsters[i]->setHealth(-1);
					monsters[i]->setBulletOrigin(bulletOrigin);
					return true;
				}
				else
				{
					collided = false;
				}
			}
			collided = false;
		}
	}

	if (!cameFromPlayer)
	{
		if (getX() + getWidth() >= player->getX() && getX() <= player->getX() + player->getWidth()) {
			if (getY() + getHeight() >= player->getY() && getY() <= player->getY() + player->getHeight()) {
				//reduce players health by 10 point
				player->takeDamage(10);
				return true;
			}
			else
			{
				collided = false;
			}
		}
		collided = false;
	}
	

	//checks for collisions with walls
	for (int i = 0; i < walls.size(); i++)
	{
		if (walls[i]->getX() <= this->getX() + this->getWidth() && walls[i]->getX() + walls[i]->getWidth() >= this->getX()) {
			if (walls[i]->getY() <= this->getY() + this->getHeight() && walls[i]->getY() + walls[i]->getHeight() >= this->getY()) {
				return true;
			}
			else
			{
				collided = false;
			}
		}
		collided = false;
	}
	return collided;
}