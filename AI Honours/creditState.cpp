#include "game.h"
#include "creditState.h"
#include "label.h"


void StateCredits::enter(){
	
}

void StateCredits::Exit(){

}

void StateCredits::update(Game &context){
	
}

void StateCredits::draw(SDL_Window * window){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f ,0.0f,0.0f, 0.0f);

	creditHeaderText->textToTexture("Credits", textFont);
	creditHeaderText->draw(roomWidth / 2 - 100.0f, roomHeight / 2 - 100.0f);

	nameText->textToTexture("Created by Mark Smellie", textFont);
	nameText->draw(roomWidth / 2 - 100.0f, roomHeight / 2);

	companyText->textToTexture("Honours Project", textFont);
	companyText->draw(roomWidth / 2 - 100.0f, roomHeight / 2 + 100.0f);

	SDL_GL_SwapWindow(window);
}

void StateCredits::handleSDLEvent(SDL_Event const &sdlEvent, Game &context){
	if (sdlEvent.type == SDL_KEYDOWN){
			context.setState(mainState);
	}
}

void StateCredits::init(Game * context){
	this->init(context);
}

void StateCredits::init(Game &context){
	
	nameText = new Label();
	companyText = new Label();
	gedText = new Label();
	creditHeaderText = new Label();

	TTF_Init();
	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
}