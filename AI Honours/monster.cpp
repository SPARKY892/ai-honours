#include "monster.h"

Monster::Monster(Player * player)
{
	this->player = player;
	health = 0;
	strength = 0;
	speed = 0;

	XPos = 0.0f;
	YPos = 0.0f;
	width = 32;
	height = 32;

	textFont = NULL;
}

int Monster::getX()
{
	return XPos;
}

int Monster::getY()
{
	return YPos;
}

int Monster::getWidth()
{
	return width;
}

int Monster::getHeight()
{
	return height;
}

int Monster::getHealth()
{
	return health;
}

int Monster::getSpeed()
{
	return speed;
}

int Monster:: getStrength()
{
	return strength;
}

void Monster::setX(int x)
{
	if (x > XPos)
	{
		setDir(RIGHT);
	}
	else if (x < XPos)
	{
		setDir(LEFT);
	}

	XPos = x;
}

void Monster::setY(int y)
{
	if (y > YPos)
	{
		setDir(DOWN);
	}
	else if (y < YPos)
	{
		setDir(UP);
	}

	YPos = y;
}

void Monster::setHealth(int Health)
{
	health += Health;
}

void Monster::setSpeed(int Speed)
{
	speed = Speed;
}

void Monster::setStrength(int Strength)
{
	strength = Strength;
}

void Monster::setDollarDrop(int dollar)
{
	dollarDrop = dollar;
}

void Monster::takeDamage(int damage)
{
	health -= damage;
}

void Monster::setTypeID(int ID)
{
	typeID = ID;
}

int Monster::getTypeID()
{
	return typeID;
}

int Monster::getDollarDrop()
{
	return dollarDrop;
}

void Monster::setDropChance(int drop)
{
	dropChance = drop;
}

int Monster::getDropChance()
{
	return dropChance;
}

void Monster::setGoal(SquareGrid::Location loc)
{
	goal = loc;
}

void Monster::setStart(SquareGrid::Location loc)
{
	start = loc;
}

void Monster::getPath()
{
	unordered_map<SquareGrid::Location, SquareGrid::Location> came_from;
	unordered_map<SquareGrid::Location, int> cost_so_far;

	a_star_search(player->getGrid(), start, goal, came_from, cost_so_far);
	path = reconstruct_path(start, goal, came_from);
}

SquareGrid::Location Monster::getNextLocation()
{
	if (path.size() > 0)
	{
		SquareGrid::Location temp = path[0];
		path.erase(path.begin());
		return temp;
	}
}

bool Monster::gotPath()
{
	return this->gotPathBool;
}

void Monster::setPathBool(bool hasPath)
{
	gotPathBool = hasPath;
}

float Monster::distance(SquareGrid::Location targetLoc)
{
	float squaredDist;
	float distance;
	int targetX = std::get<0>(targetLoc);
	int targetY = std::get<1>(targetLoc);
	int xpos = this->getX() / 32;
	int ypos = this->getY() / 32;

	distance = abs(xpos - targetX) + abs(ypos - targetY);
	/*squaredDist = pow(targetX - xpos, 2) + pow(targetY - ypos, 2);
	distance = sqrt(squaredDist);*/
	return distance;
}

void Monster::setDir(direction dir)
{
	currentDir = dir;
}

void Monster::advancePath()
{
	SquareGrid::Location nextLoc = getNextLocation();

	int targetX = std::get<0>(nextLoc) * 32;
	int targetY = std::get<1>(nextLoc) * 32;

	if (targetX >= 0 && targetY >= 0)
	{
		setX(targetX);
		setY(targetY);
	}
}

bool Monster::spawnCheck()
{
	tuple<int, int> tempLoc{ getX() / 32, getY() / 32 };

	if (player->getGrid().walls.count(tempLoc) != 0)
	{
		return true;
	}

	return false;
}
