#include "Brute.h"

Brute::Brute(Player * player) : Monster(player)
{
	this->player = player;

	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	targetText = new Label();

	setHealth(4);
	setStrength(1);
	setTypeID(2);
	setGoal(player->getLocation());
	setRange(8);

	startTime = clock();
	bulletFireTime = clock();

	
}

void Brute::draw()
{
	//draws monster depending on current direction
	glColor3f(0.0f, 1.0f, 1.0f);
	if (getDir() == UP)
	{
		glBegin(GL_TRIANGLES);
		glVertex2f(getX(), getY() + getHeight());
		glVertex2f(getX() + getWidth(), getY() + getHeight());
		glVertex2f(getX() + getWidth() / 2, getY());
		glEnd();
	}
	else if (getDir() == LEFT)
	{
		glBegin(GL_TRIANGLES);
		glVertex2f(getX() + getWidth(), getY());
		glVertex2f(getX() + getWidth(), getY() + getHeight());
		glVertex2f(getX(), getY() + getHeight() / 2);
		glEnd();
	}
	else if (getDir() == DOWN)
	{
		glBegin(GL_TRIANGLES);
		glVertex2f(getX(), getY());
		glVertex2f(getX() + getWidth(), getY());
		glVertex2f(getX() + getWidth() / 2, getY() + getHeight());
		glEnd();
	}
	else if (getDir() == RIGHT)
	{
		glBegin(GL_TRIANGLES);
		glVertex2f(getX(), getY());
		glVertex2f(getX(), getY() + getHeight());
		glVertex2f(getX() + getWidth(), getY() + getHeight() / 2);
		glEnd();
	}
	
	//selects colour for health bar depending on health value
	switch (getHealth())
	{
	case 4:
		glColor3f(0.0f, 1.0f, 0.0f);
		break;
	case 3:
		glColor3f(1.0f, 0.87f, 0.0f);
		break;
	case 2:
		glColor3f(1.0f, 0.65f, 0.0f);
		break;
	case 1:
		glColor3f(1.0f, 0.0f, 0.0f);
	}

	//draw health bar
	glBegin(GL_POLYGON);
	glVertex2f(getX(), getY() - 10);
	glVertex2f(getX() + (getHealth() * 8), getY() - 10);
	glVertex2f(getX() + (getHealth() * 8), getY() - 5);
	glVertex2f(getX(), getY() - 5);
	glEnd();
}

bool Brute::collisionCheck()
{
	//checks for collisions with the player
	if(player->getX() <= getX() + getWidth() && player->getX() + player->getWidth() >= getX()){
		if (player->getY() <= getY() + getHeight() && player->getY() + player->getHeight() >= getY()) {
			player->takeDamage(1);
			return true;
		}
	}

 	return false;	
}

bool Brute::fireBullet()
{
	clock_t diff = clock() - bulletFireTime;
	int ms = diff * 1000 / CLOCKS_PER_SEC;

	if (distance(player->getLocation()) <= getRange() && ms >= 500 )
	{
		bulletFireTime = clock();
		return true;
	}

	return false;
}


void Brute::update()
{
	//Checks time between start of timer and current time
	clock_t diff = clock() - startTime;
	int ms = diff * 1000 / CLOCKS_PER_SEC;

	//retrieves start and goal for pathfinding
	SquareGrid::Location goal = getGoal();
	SquareGrid::Location start = getStart();

	int dist = distance(player->getLocation());

	//checks if distance is less than root 32
	if (dist == 1)
	{
		if (player->getX() < getX())
		{
			setDir(LEFT);
		}
		else if (player->getX() > getX())
		{
			setDir(RIGHT);
		}
		else if (player->getY() < getY())
		{
			setDir(UP);
		}
		else if (player->getY() > getY())
		{
			setDir(DOWN);
		}
	}


	if (dist <= getRange() && dist > 1)
	{
		if (player->getX() / 32 != std::get<0>(goal) || player->getY() / 32 != std::get<1>(goal))
		{
			setStart(getLocation());
			setGoal(player->getLocation());
			getPath();
		}

		//if timer is greater than 0.1 seconds the position will update to help smooth out movement
		if (ms >= 100 && start != goal)
		{
			advancePath();
			startTime = clock();
		}
	}
}
