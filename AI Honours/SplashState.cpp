#include "game.h"
#include "SplashState.h"
#include "label.h"

void SplashState::enter(){
	
}

void SplashState::Exit(){

}

void SplashState::update(Game &context){
	if(SDL_GetTicks() >= 3000)
	{
		context.setState(mainState);
	}
}

void SplashState::draw(SDL_Window * window){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f ,0.0f,0.0f, 0.0f);
	
	logoText->textToTexture("AI Honours Project", textFont);
	logoText->draw(roomWidth / 2 - 100.0f, roomHeight / 2);

	SDL_GL_SwapWindow(window);
}

void SplashState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context){
	if (sdlEvent.type == SDL_KEYDOWN){
		context.setState(mainState);
	}
}

void SplashState::init(Game * context){
	this->init(context);
}

void SplashState::init(Game &context){
	
	logoText = new Label();


	TTF_Init();
	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
}
