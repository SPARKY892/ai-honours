#include "game.h"
#include "SimpleAIState.h"
#include "label.h"
#include "Item.h"


SimpleAIState::SimpleAIState()
{
	player = new Player();

	monsters.push_back(new Brute(player));
	monsters.push_back(new Brute(player));
	monsters.push_back(new Brute(player));

	//perimeter walls
	walls.push_back(new Wall(player, 0, 0, 1, 24));
	walls.push_back(new Wall(player, 0, 0, 29, 1));
	walls.push_back(new Wall(player, 28, 0, 1, 24));
	walls.push_back(new Wall(player, 0, 23, 29, 1));

	//top left L block
	walls.push_back(new Wall(player, 3, 8, 6, 1));
	walls.push_back(new Wall(player, 8, 3, 1, 6));

	//top right L block
	walls.push_back(new Wall(player, 20, 8, 6, 1));
	walls.push_back(new Wall(player, 20, 3, 1, 6));

	//Centre block
	walls.push_back(new Wall(player, 11, 11, 7, 3));

	//bottom left L block
	walls.push_back(new Wall(player, 3, 15, 6, 1));
	walls.push_back(new Wall(player, 8, 15, 1, 6));

	//bottom right L block
	walls.push_back(new Wall(player, 20, 15, 6, 1));
	walls.push_back(new Wall(player, 20, 15, 1, 6));


	monsterToDelete = NULL;
	itemToDelete = NULL;
	deadMonsterCount = NULL;
	canContinue = false;

	for(unsigned int i = 0; i < monsters.size(); i++)
	{
		do
		{
			//(rand() % ((max - min) + min) / 32
			int maxX = roomWidth - 32;
			int maxY = roomHeight - 32;
			int min = 32;

			int randX = (rand() % ((maxX - min) + min));
			int randY = (rand() % ((maxY - min) + min));

			monsters[i]->setX(nearestMult(randX, 32));
			monsters[i]->setY(nearestMult(randY, 32));
		}
		while (monsters[i]->spawnCheck() || monsters[i]->distance(player->getLocation()) <= monsters[i]->getRange() );
	}
}

//calculates nearest multiple
int SimpleAIState::nearestMult(int number, int multiple)
{
	return ((number + (multiple / 2)) / multiple ) * multiple;
}

void SimpleAIState::enter()
{	

	if(!gameStarted)
	{
		gameStarted = true;
	}

	if(monsterToDelete != NULL)
	{
		delete monsters[monsterToDelete - 1];
		monsters[monsterToDelete - 1] = NULL;
	}
}

void SimpleAIState::Exit()
{
}

//performs pdate calls for all objects in the game
void SimpleAIState::update(Game &context)
{
	//loop monsters vector performing updates, pathfinding and collision checks
	for(unsigned int i = 0; i < monsters.size(); i++)
	{
		if(monsters[i] != NULL)
		{
			if (!monsters[i]->gotPath()) {
				monsters[i]->setStart(monsters[i]->getLocation());
				monsters[i]->getPath();
				monsters[i]->setPathBool(true);
			}

			monsters[i]->collisionCheck();
			monsters[i]->update();

			if (monsters[i]->fireBullet())
			{
				bullets.push_back(new Bullet(player, walls, monsters, monsters[i]->getDir(), false, monsters[i]));
			}
		}
	}

	//update bullet positions, check for collisions and delete any that collide
	for (unsigned int i = 0; i < bullets.size(); i++)
	{
		if (bullets[i] != NULL)
		{
			bullets[i]->update();

			if (bullets[i]->collisionCheck())
			{
				delete bullets[i];
				bullets.erase(bullets.begin() + i);
			}
		}
	}

	//check for monsters with 0 health or less and delete them
	for (unsigned int i = 0; i < monsters.size(); i++)
	{
		if (monsters[i]->getHealth() <= 0)
		{
			delete monsters[i];
			monsters.erase(monsters.begin() + i);
		}
	}

	//check if player is alive with no more monsters or dead
	if (player->getHealth() > 0 && monsters.empty() == true)
	{
		context.setState(battleFinishState);
	}
	else if (player->getHealth() <= 0)
	{
		context.setState(battleFinishState);
	}
}

//handles drawing for all objects in the game
void SimpleAIState::draw(SDL_Window * window)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window	
	
	//draw player
	player->drawPlayer();
	canContinue = true;

	//loop walls vector to draw them
	for (unsigned int i = 0; i < walls.size(); i++)
	{
		if (walls[i] != NULL)
		{
			walls[i]->draw();
		}
	}

	//loop monsters vector to draw them
	for(unsigned int i = 0; i < monsters.size(); i++)
	{
		if(monsters[i] != NULL)
		{
			monsters[i]->draw();
		}
	}
	
	//loop bullets vector to draw them
	for (unsigned int i = 0; i < bullets.size(); i++)
	{
		if (bullets[i] != NULL)
		{
			bullets[i]->draw();
		}
	}


    glColor3f(1.0,1.0,1.0);
    currentTime = clock();
    // On some systems, CLOCKS_PER_SECOND is 1000, which makes the arithmetic below redundant
    // - but this is not necessarily the case on all systems
    float milliSecondsPerFrame = ((currentTime - lastTime)/(float)CLOCKS_PER_SEC*1000);

    lastTime = clock();

	std::stringstream strStreamPlayerStats;

	//puts the players health, strength and speed into a stringstream and displays it on screen
	strStreamPlayerStats << "Health: " << player->getHealth();
	Label playerStream;
	playerStream.textToTexture(strStreamPlayerStats.str().c_str(), textFont);
	playerStream.draw(32.0f, 30.0f);

	SDL_GL_SwapWindow(window); // swap buffers
}

bool SimpleAIState::getGameFinished()
{
	if(monsters.empty() && player->getHealth() > 0)
	{
		return true;
	}
}

void SimpleAIState::init(Game * context)
{
	this->init(context);
}

void SimpleAIState::init(Game &context){
	glClearColor(0.0, 0.0, 0.0, 0.0);

	lastTime = clock();	
	scoreText = new Label();

	//set up TrueType / SDL_ttf
	TTF_Init();
	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	
}

void SimpleAIState::handleSDLEvent(SDL_Event const &sdlEvent, Game& context)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		switch( sdlEvent.key.keysym.sym )
		{
		case SDLK_ESCAPE:
			context.setState(mainState); break;
			case SDLK_UP:
			case 'w': case 'W': 
				player->moveY(-32);
				player->setDir(UP);
				break;
			case SDLK_DOWN:
			case 's': case 'S':
				player->moveY(32);
				player->setDir(DOWN);
				break;
			case SDLK_LEFT:
			case 'a': case 'A': 
				player->moveX(-32);
				player->setDir(LEFT);
				break;
			case SDLK_RIGHT:
			case 'd': case 'D':
				player->moveX(32);
				player->setDir(RIGHT);
				break;
			default:
				break;
			case SDLK_SPACE:
				bullets.push_back(new Bullet(player, walls, monsters, player->getDir(), true));
		}
	}
}

SimpleAIState::~SimpleAIState()
{
	delete player;

	for(unsigned int i = 0; i < monsters.size(); i++)
	{
		delete monsters[i];
	}

	for (unsigned int i = 0; i < walls.size(); i++)
	{
		delete walls[i];
	}
}

