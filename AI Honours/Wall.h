#pragma once
#include "Common.h"
#include "Player.h"

class Wall
{
public:
	Wall(Player *, int x, int y, int width, int height);
	void draw();

	int getX() { return x; }
	int getY() { return y; }
	int getWidth() { return width; }
	int getHeight() { return height; }

private:
	Player * player;
	int x, y, width, height;
};

