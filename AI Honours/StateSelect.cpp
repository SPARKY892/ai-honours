#include "game.h"
#include "StateSelect.h"
#include "label.h"

void SelectState::enter(){
	
}

void SelectState::Exit(){

}

void SelectState::update(Game &context){

}

void SelectState::draw(SDL_Window * window){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f ,0.0f,0.0f, 0.0f);
	
	logoText->textToTexture("Level Selection", textFont);
	logoText->draw(roomWidth / 2 - 100.0f, roomHeight / 2 - 100.0f);

	level1->textToTexture("Simple AI [1]", textFont);
	level1->draw(roomWidth / 2 - 100.0f, roomHeight / 2);

	level2->textToTexture("Advanced AI [2]", textFont);
	level2->draw(roomWidth / 2 - 100.0f, roomHeight / 2 + 100.0f);

	SDL_GL_SwapWindow(window);
}

void SelectState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context){	
	if (sdlEvent.type == SDL_KEYDOWN)
	{	
		switch( sdlEvent.key.keysym.sym )
		{
		case SDLK_1:
			if (simpleAIState != NULL) {
				delete simpleAIState;
				simpleAIState = new SimpleAIState();
			}
			
			((AdvancedAIState*)advancedAIState)->setCanContinue(false);
			
			context.setState(simpleAIState);	
			break;
		case SDLK_2:
			if (advancedAIState != NULL) {
				delete advancedAIState;
				advancedAIState = new AdvancedAIState();
			}

			((SimpleAIState*)simpleAIState)->setCanContinue(false);

			context.setState(advancedAIState);
			break;
		case SDLK_ESCAPE:
			context.setState(mainState);
			break;
		}
	}
}

void SelectState::init(Game * context){
	this->init(context);
}

void SelectState::init(Game &context){
	
	logoText = new Label();
	level1 = new Label();
	level2 = new Label();

	TTF_Init();
	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
}