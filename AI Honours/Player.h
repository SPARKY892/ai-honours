#pragma once
#include "Common.h"
#include"GameState.h"
#include "label.h"

enum direction { UP, DOWN, LEFT, RIGHT };



class Player
{
public:
	Player();

	void drawPlayer();
	void moveX(int dx);
	void moveY(int dy);

	int getX();
	int getY();
	int getWidth();
	int getHeight();
	int getHealth();
	
	void takeDamage(int);
	void healthPack();

	GridWithWeights getGrid() { return gameGrid; };
	
	void setDir(direction dir) { currentDir = dir; }
	direction getDir() { return currentDir; }
	SquareGrid::Location getLocation() { return std::make_tuple(getX()/32, getY()/32); }

	point getPoint1() { return point1; }
	point getPoint2() { return point2; }
	point getPoint3() { return point3; }
	
private:

	int xpos;
	int ypos;
	int width;
	int height;

	int health, speed, strength;
	int dollars;
	int maxHealth;
	
	Label * playerText;
	TTF_Font* textFont;	// SDL type for True-Type font rendering

	GridWithWeights gameGrid = make_diagram4();
	direction currentDir;
	
	point point1;
	point point2;
	point point3;
};

