#pragma once

#include <GL\glew.h>
#include <SDL_ttf.h>
#include <SDL.h>
#include <GL\GL.h>
#include <vector>
// C stdlib and C time libraries for rand and time functions
#include <cstdlib>
#include <ctime>
// iostream for cin and cout
#include <iostream>
// stringstream and string
#include <sstream>
#include <string>
#include <tuple>
#include "implementation.h"

using std::string;
using std::vector;

struct point
{
	int x;
	int y;
};