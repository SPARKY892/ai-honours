#include "game.h"
#include "BattleFinishState.h"
#include "SimpleAIState.h"
#include "AdvancedAIState.h"

void BattleFinishState::enter(){
	
}

void BattleFinishState::Exit(){

}

void BattleFinishState::update(Game &context){

}

void BattleFinishState::draw(SDL_Window * window){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f ,0.0f,0.0f, 0.0f);


	if (((SimpleAIState*)simpleAIState)->getGameFinished() == true && ((SimpleAIState*)simpleAIState)->getCanContinue() == true)
	{
		winText->textToTexture("You won the game!", textFont);
		winText->draw(320.0f, 320.0f);
	}

	if(((SimpleAIState*)simpleAIState)->getGameFinished() == false && ((SimpleAIState*)simpleAIState)->getCanContinue() == true)
	{	
		gameOverText->textToTexture("Game Over!", textFont);
		gameOverText->draw(320.0f, 320.0f);
	}

	if (((AdvancedAIState*)advancedAIState)->getGameFinished() == true && ((AdvancedAIState*)advancedAIState)->getCanContinue() == true)
	{
		winText->textToTexture("You won the game!", textFont);
		winText->draw(320.0f, 320.0f);
	}

	if (((AdvancedAIState*)advancedAIState)->getGameFinished() == false && ((AdvancedAIState*)advancedAIState)->getCanContinue() == true)
	{
		gameOverText->textToTexture("Game Over!", textFont);
		gameOverText->draw(320.0f, 320.0f);
	}


	SDL_GL_SwapWindow(window);
}

void BattleFinishState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context){
	if (sdlEvent.type == SDL_KEYDOWN){
		switch( sdlEvent.key.keysym.sym)
		{
		case SDLK_SPACE:
				context.setState(mainState);
		}
	}
}

void BattleFinishState::init(Game * context){
	this->init(context);
}

void BattleFinishState::init(Game &context){
	
	winText = new Label();
	gameOverText = new Label();
	itemText = new Label();

	TTF_Init();
	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
}
