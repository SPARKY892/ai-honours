#pragma once

#include "Common.h"
#include "GameState.h"
#include "label.h"

class StateCredits: public GameState
{
public:
	void draw(SDL_Window * window);

	void init(Game * context);
	void init(Game &context);
	void update(Game &context);
	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);
	void enter();
	void Exit();
	

private:
	Label * creditHeaderText;
	Label * nameText;
	Label * companyText;
	Label * gedText;
	TTF_Font* textFont;	// SDL type for True-Type font rendering
	Game * context;	
};