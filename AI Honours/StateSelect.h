#pragma once

#include "Common.h"
#include "GameState.h"
#include "label.h"
#include "SimpleAIState.h"
#include "AdvancedAIState.h"

class SelectState: public GameState
{
public:
	void draw(SDL_Window * window);
	void init(Game * context);
	void init(Game &context);
	void update(Game &context);
	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);
	void enter();
	void Exit();
	

private:
	Label * logoText;
	Label * level1;
	Label * level2;
	TTF_Font* textFont;	// SDL type for True-Type font rendering
	Game * context;	
};