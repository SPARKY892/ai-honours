#pragma once

#include "Common.h"

class Label
{
public:

	Label();
	~Label();
	int getHeight();
	int getWidth();
	int getID();

	void textToTexture(std::string, TTF_Font*);
	void draw(float, float);

private:

	GLuint texID;
	int height;
	int width;
};