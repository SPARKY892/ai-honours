#pragma once
#include "Common.h"
#include "label.h"
#include "Item.h"

class ItemHealthPack: public Item
{
public:
	ItemHealthPack(Player*);
	~ItemHealthPack();

	void draw();
	int update();

private:
	Label * itemText;
	TTF_Font * textFont;
};
