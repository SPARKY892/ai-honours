#include "ItemHealthPack.h"


ItemHealthPack::ItemHealthPack(Player * player) : Item(player)
{
	this->player = player;
	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	itemText = new Label();
}

void ItemHealthPack::draw()
{
	// draw target
    glColor3f(1.0,0.0,0.0);
    glBegin(GL_POLYGON);
      glVertex3f (this->getX(), this->getY(), 0.0); // first corner
      glVertex3f (this->getX()+this->getWidth(), this->getY(), 0.0); // second corner
      glVertex3f (this->getX()+this->getWidth(), this->getY()+this->getHeight(), 0.0); // third corner
      glVertex3f (this->getX(), this->getY()+this->getHeight(), 0.0); // fourth corner
    glEnd();

	itemText->textToTexture("Health Pack", textFont);
	itemText->draw(this->getX()+(this->getWidth()/2.0f), this->getY()+this->getHeight());
}

int ItemHealthPack::update()
{
	if(player->getX() <= this->getX() + this->getWidth() && player->getX() + player->getWidth() >= this->getX()){
		if(player->getY() <= this->getY()+ this->getHeight() && player->getY() + player->getHeight() >= this->getY()){
			return 3;
		}else{
			return 0;
		}
	}
	return 0;
}

ItemHealthPack::~ItemHealthPack(void)
{
}
